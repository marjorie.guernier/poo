/*
Theme Name: 	Gaion Child Theme
Theme Uri:  	http://demo.wpthemego.com/themes/sw_gaion/
Description:  	A child theme of SW Gaion
Author:     	magentech
Author Uri: 	https://themeforest.net/user/magentech/
Template:   	gaion
Version:    	1.0.0
License:    	GNU General Public License v2 or later
*/


/*****  HEADER  ******/

.header-right-top .widget_custom_html .block-header h4 {
    padding: 9px 0px;
    color: white;
}

.header-top .header-logo {
    background-color: #48545b;
}


/*****  ACCUEIL  ******/

.homeProducts.wpb_column.vc_column_container.vc_col-sm-12.vc_col-has-fill h3 {
    color: white;
}

.sw-woo-tab-default .resp-tab .category-slider-content .nav-tabs-select ul.nav-tabs li.active>a,
.sw-woo-tab-default .resp-tab .category-slider-content .nav-tabs-select ul.nav-tabs li:hover>a,
.sw-woo-tab-cat .resp-tab .category-slider-content .nav-tabs-select ul.nav-tabs li.active>a,
.sw-woo-tab-cat .resp-tab .category-slider-content .nav-tabs-select ul.nav-tabs li:hover>a {
    color: #fff;
    background: #b6cd0d;
    border-color: #b6cd0d;
}

.vc_btn3-container.btnAboutHome.vc_btn3-center>button {
    background-color: white;
    color: #48545b;
    text-transform: uppercase;
    font-weight: 600;
}


/*Home page about text*/

p.homeAboutp1 {
    width: 400px;
    margin-left: 240px;
}

p.homeAboutp2 {
    width: 400px;
    margin-left: 240px;
}

p.homeAboutp3 {
    width: 400px;
    margin-left: 240px;
}

@media screen and (min-width: 950px) and (max-width: 1100px) {
    p.homeAboutp1 {
        width: 390px;
        margin-left: 20px;
    }
    p.homeAboutp2 {
        width: 390px;
        margin-left: 20px;
    }
    p.homeAboutp3 {
        width: 390px;
        margin-left: 20px;
    }
}

@media screen and (min-width: 1100px) and (max-width: 1200px) {
    p.homeAboutp1 {
        width: 390px;
        margin-left: 80px;
    }
    p.homeAboutp2 {
        width: 390px;
        margin-left: 80px;
    }
    p.homeAboutp3 {
        width: 390px;
        margin-left: 80px;
    }
}

@media screen and (min-width: 1200px) and (max-width: 1500px) {
    p.homeAboutp1 {
        width: 390px;
        margin-left: 130px;
    }
    p.homeAboutp2 {
        width: 390px;
        margin-left: 130px;
    }
    p.homeAboutp3 {
        width: 390px;
        margin-left: 130px;
    }
}

@media screen and (min-width: 360px) and (max-width: 800px) {
    p.homeAboutp1 {
        width: 390px;
        margin-left: 0px;
    }
    p.homeAboutp2 {
        width: 390px;
        margin-left: 0px;
    }
    p.homeAboutp3 {
        width: 390px;
        margin-left: 0px;
    }
}

@media only screen and (max-width: 320px) {
    p.homeAboutp1 {
        width: 280px;
        margin-left: 0px;
    }
    p.homeAboutp2 {
        width: 280px;
        margin-left: 0px;
    }
    p.homeAboutp3 {
        width: 280px;
        margin-left: 0px;
    }
}

.vc_btn3-container.btnAboutHome.vc_btn3-center>button:hover {
    background-color: #48545b;
    color: #b6cd0d;
    text-transform: uppercase;
    font-weight: 600;
    border-color: #48545b;
}

.vc_row.vc_column-gap-10>.vc_column_container {
    padding: 0 px;
}

.marqueSlider {
    padding: 0px;
    border: 0px;
}

.testimonial-slider.carousel.slide.layout1.sliderTestimonial {
    background-color: #b6cd0d;
}

.testimonial-slider.layout1 .wrap-content .carousel-indicators li.active {
    background-color: #b6cd0d;
    border: 0;
}

.testimonial-slider.layout1 .wrap-content .carousel-inner .item .client-say-info .name-client h2 a {
    color: #b6cd0d;
}


/*****  FOOTER  ******/

.inputNewsletterFooter {
    display: flex;
}

button.btnNewsletterFooter {
    width: 85px;
    height: 50px;
    border-radius: none;
    background-color: #b6cd0d;
    color: white;
    text-transform: uppercase;
    border-radius: 0px;
}

input.inpputNewsletterFooter {
    padding-right: 200px;
    height: 50px;
    border-radius: 0px;
}

.footer-right-block {
    display: flex;
}

.socials-footer ul li a {
    color: #b6cd0d;
}

.socials-footer ul li a:hover {
    background-color: #48545b;
}

.wpb_single_image.wpb_content_element.vc_align_left.footerLogo figure a img {
    width: 120px;
}
